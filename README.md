# Seven admin theme

This is a sub theme of Seven. 

Do not sub-theme this sub-theme, just clone it, remove the .git and then bend it to your will.
```
cd /themes/custom/seven_admin_subtheme
rm -rf .git
```

If you really want to rename it, do the following (run from your Mac)
#### Step 1

```
brew install rename
cd /themes/custom/seven_admin_subtheme
rename 's/seven_admin_subtheme/my_admin_theme_name/' *
cd config/optional
rename 's/seven_admin_subtheme/my_admin_theme_name/' *
```
#### Step 2
Open up PHP Storm, right click on the theme folder, select "Replace in path"
```
find: seven_admin_subtheme
replace: my_admin_theme_name
```

#### Step 3
Edit the `name` and `description` in my_admin_theme_name.info.yml

#### Step 4
Clear the cache

```
(ﾉ´ヮ´)ﾉ*:･ﾟ✧✧✧ enjoy ✧✧ 
```
